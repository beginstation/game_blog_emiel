<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index/index.html.twig */
class __TwigTemplate_905d90993ab12a963028174e1951d2123f38b35650345ac019b82c0e0156277b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "index/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "index/index.html.twig"));

        // line 1
        $this->displayBlock('body', $context, $blocks);
        // line 24
        echo "
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 1
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 2
        echo "    <div class=\"container\">
        ";
        // line 3
        $this->loadTemplate("navbar.html.twig", "index/index.html.twig", 3)->display($context);
        // line 4
        echo "        <div class=\"content\">
            <div class=\"blogpostpreview\">
                <div class=\"blogpostpreview-content\">
                    <div class=\"blogpostpreview-title\">
                        Insert title here
                    </div>
                    <div class=\"blogpostpreview-text\">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam facilisis quam vitae lorem cursus, quis venenatis turpis condimentum. Mauris quis mauris vel elit dictum accumsan at eu elit. Integer quis ligula sit amet augue accumsan auctor. Quisque auctor velit in erat fermentum facilisis. Nam tincidunt risus sit amet arcu cursus, sit amet auctor nunc lacinia. Nullam sed vehicula risus. Nunc maximus justo eget turpis laoreet, a ornare lacus maximus. Maecenas tempus nibh pulvinar lectus iaculis pharetra.
                    </div>
                    <div class=\"blogpostpreview-more\">
                        <a href=\"# \">Lees meer..</a>
                    </div>
                </div>
                <div class=\"blogpostpreview-image\">
                    <img src=\"https://static.apparata.nl/images/2018/fifa-ea-games.jpg\" alt=\"\">
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "index/index.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  72 => 4,  70 => 3,  67 => 2,  57 => 1,  46 => 24,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% block body %}
    <div class=\"container\">
        {% include \"navbar.html.twig\" %}
        <div class=\"content\">
            <div class=\"blogpostpreview\">
                <div class=\"blogpostpreview-content\">
                    <div class=\"blogpostpreview-title\">
                        Insert title here
                    </div>
                    <div class=\"blogpostpreview-text\">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam facilisis quam vitae lorem cursus, quis venenatis turpis condimentum. Mauris quis mauris vel elit dictum accumsan at eu elit. Integer quis ligula sit amet augue accumsan auctor. Quisque auctor velit in erat fermentum facilisis. Nam tincidunt risus sit amet arcu cursus, sit amet auctor nunc lacinia. Nullam sed vehicula risus. Nunc maximus justo eget turpis laoreet, a ornare lacus maximus. Maecenas tempus nibh pulvinar lectus iaculis pharetra.
                    </div>
                    <div class=\"blogpostpreview-more\">
                        <a href=\"# \">Lees meer..</a>
                    </div>
                </div>
                <div class=\"blogpostpreview-image\">
                    <img src=\"https://static.apparata.nl/images/2018/fifa-ea-games.jpg\" alt=\"\">
                </div>
            </div>
        </div>
    </div>
{% endblock %}

", "index/index.html.twig", "/var/www/php/sites/game_blog_emiel/templates/index/index.html.twig");
    }
}
