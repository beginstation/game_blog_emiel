<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* navbar.html.twig */
class __TwigTemplate_c0c248a9ac748524de640f948c6dd54da1498619a1bb52e4001e340952b0a04a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "navbar.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "navbar.html.twig"));

        // line 1
        echo "<head>
    <link rel=\"stylesheet\" href=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/app.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">
    <link rel=\"preconnect\" href=\"https://fonts.gstatic.com\">
    <link href=\"https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap\" rel=\"stylesheet\"> 
</head>

<div class=\"nav-container\">
    <div class=\"nav-title\">
        <h1>Senjux gaming</h1>

       
    </div>
    <nav>
        <div class=\"nav-content\">
            <div class=\"nav-left\">
                <a href=\"/\">Homepage</a>
                <div class=\"dropdown\">
                    <button class=\"dropbtn\">
                        Playstation
                        <i class=\"fa fa-caret-down\"></i>
                    </button>
                    <div class=\"dropdown-content\">
                        <a href=\"/playstation/hardware\">Hardware</a>
                        <a href=\"/playstation/games\">Games</a>
                    </div>
                </div>
                <div class=\"dropdown\">
                    <button class=\"dropbtn\">
                        Xbox
                        <i class=\"fa fa-caret-down\"></i>
                    </button>
                    <div class=\"dropdown-content\">
                        <a href=\"/playstation/hardware\">Hardware</a>
                        <a href=\"/playstation/games\">Games</a>
                    </div>
                </div>
                <div class=\"dropdown\">
                    <button class=\"dropbtn\">
                        Nintendo
                        <i class=\"fa fa-caret-down\"></i>
                    </button>
                    <div class=\"dropdown-content\">
                        <a href=\"/playstation/hardware\">Hardware</a>
                        <a href=\"/playstation/games\">Games</a>
                    </div>
                </div>
                <div class=\"dropdown\">
                    <button class=\"dropbtn\">
                        PC
                        <i class=\"fa fa-caret-down\"></i>
                    </button>
                    <div class=\"dropdown-content\">
                        <a href=\"/playstation/hardware\">Hardware</a>
                        <a href=\"/playstation/games\">Games</a>
                    </div>
                </div>
            </div>
            <div class=\"nav-right\">
                ";
        // line 60
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 60, $this->source); })()), "user", [], "any", false, false, false, 60)) {
            // line 61
            echo "                    <div class=\"nav-logout\">
                        You are logged in as ";
            // line 62
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 62, $this->source); })()), "user", [], "any", false, false, false, 62), "username", [], "any", false, false, false, 62), "html", null, true);
            echo ",
                        <a href=\"";
            // line 63
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logout");
            echo "\">Logout</a>
                    </div>
                ";
        } else {
            // line 66
            echo "                    <a href=\"/login\">Login</a>
                    <a href=\"/register\">Register</a>
                ";
        }
        // line 69
        echo "            </div>
        </div>
    </nav>
</div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "navbar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 69,  122 => 66,  116 => 63,  112 => 62,  109 => 61,  107 => 60,  46 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<head>
    <link rel=\"stylesheet\" href=\"{{ asset('build/app.css') }}\" />
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">
    <link rel=\"preconnect\" href=\"https://fonts.gstatic.com\">
    <link href=\"https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap\" rel=\"stylesheet\"> 
</head>

<div class=\"nav-container\">
    <div class=\"nav-title\">
        <h1>Senjux gaming</h1>

       
    </div>
    <nav>
        <div class=\"nav-content\">
            <div class=\"nav-left\">
                <a href=\"/\">Homepage</a>
                <div class=\"dropdown\">
                    <button class=\"dropbtn\">
                        Playstation
                        <i class=\"fa fa-caret-down\"></i>
                    </button>
                    <div class=\"dropdown-content\">
                        <a href=\"/playstation/hardware\">Hardware</a>
                        <a href=\"/playstation/games\">Games</a>
                    </div>
                </div>
                <div class=\"dropdown\">
                    <button class=\"dropbtn\">
                        Xbox
                        <i class=\"fa fa-caret-down\"></i>
                    </button>
                    <div class=\"dropdown-content\">
                        <a href=\"/playstation/hardware\">Hardware</a>
                        <a href=\"/playstation/games\">Games</a>
                    </div>
                </div>
                <div class=\"dropdown\">
                    <button class=\"dropbtn\">
                        Nintendo
                        <i class=\"fa fa-caret-down\"></i>
                    </button>
                    <div class=\"dropdown-content\">
                        <a href=\"/playstation/hardware\">Hardware</a>
                        <a href=\"/playstation/games\">Games</a>
                    </div>
                </div>
                <div class=\"dropdown\">
                    <button class=\"dropbtn\">
                        PC
                        <i class=\"fa fa-caret-down\"></i>
                    </button>
                    <div class=\"dropdown-content\">
                        <a href=\"/playstation/hardware\">Hardware</a>
                        <a href=\"/playstation/games\">Games</a>
                    </div>
                </div>
            </div>
            <div class=\"nav-right\">
                {% if app.user %}
                    <div class=\"nav-logout\">
                        You are logged in as {{ app.user.username }},
                        <a href=\"{{ path('app_logout') }}\">Logout</a>
                    </div>
                {% else %}
                    <a href=\"/login\">Login</a>
                    <a href=\"/register\">Register</a>
                {% endif %}
            </div>
        </div>
    </nav>
</div>", "navbar.html.twig", "/var/www/php/sites/game_blog_emiel/templates/navbar.html.twig");
    }
}
