<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\User;
use App\Form\AddPostType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Gedmo\Sluggable\Util\Urlizer;



class PostController extends AbstractController
{
    /**
     * @Route("/addpost", name="post")
     */
    public function index(Request $request): Response
    {
        $post = new Post();
        
        $user = $this->getUser();

        // $post = $this->getDoctrine()
        //     ->getRepository(Post::class)
        //     ->find($user->getId());

        //     dd($id);

        // dd($post);

        $form = $this->createForm(AddPostType::class, $post);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $post = $form->getData();

            /**@var UploadedFile $uploadedFile */
            $uploadedFile = $form['image']->getData();
            $destination = $this->getParameter('kernel.project_dir').'/public/uploads/post_image';
            
            $originalFilename = pathinfo($uploadedFile->getClientOriginalname(), PATHINFO_FILENAME);
            $newFileName = Urlizer::urlize($originalFilename).'-'.uniqid().'.'.$uploadedFile->guessExtension();

            $uploadedFile->move(
                $destination,
                $newFileName
            );

            $post->setPostImg($newFileName);
            $post->setUserId($user);

            // $userId = $user->getUser()->getId();
            // $post->setUserId($userId);

            // dd($post);
         
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($post);
            $entityManager->flush();
        }
        

        return $this->render('post/index.html.twig', [
            'controller_name' => 'PostController',
            'postForm' => $form->createView(),
        ]);
    }
}
