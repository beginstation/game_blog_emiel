<?php

namespace App\Form;

use App\Entity\Post;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class AddPostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('postTitle', TextType::class, [
                'label' => 'Title',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please fill in a title for your blogpost!',
                    ]),
                    new Length([
                        'min' => 10,
                        'minMessage' => 'Please make sure your title is at least 10 characters long!',
                        'max' => 150,
                        'maxMessage' => 'Please make sure your title is not longer than 150 characters!'
                    ])
                ]
                
            ])
            ->add('postContentSummary', TextareaType::class, [
                'label' => 'Summary',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please fill in a summary for your blogpost!',
                    ]),
                    new Length([
                        'min' => 10,
                        'minMessage' => 'Please make sure your blog summary is at least 10 characters long!',
                        'max' => 255,
                        'maxMessage' => 'Please make sure your blog summary is not longer than 255 characters!'
                    ])
                ]
                
            ])
            ->add('postContentFull', TextareaType::class, [
                'label' => 'Content',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please fill in the content of your blogpost!',
                    ]),
                    new Length([
                        'min' => 10,
                        'minMessage' => 'Please make sure your blog content is at least 10 characters long!',
                    ])
                ]
                
            ])
            ->add('image', FileType::class, [
                'mapped' => false,
                'label' => 'Image',
            ])
            // ->add('postTypeId', ChoiceType::class, [
            //     'label' => 'category',
            //     'constraints' => [
                    
            //     ]
                
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}
