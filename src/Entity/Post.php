<?php

namespace App\Entity;

use App\Repository\PostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\DateTime;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PostRepository::class)
 */
class Post
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $postTitle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $postContentSummary;

    /**
     * @ORM\Column(type="text")
     */
    private $postContentFull;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $postLikes;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $postDislikes;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datePosted;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="postId")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userId;

    /**
     * @ORM\OneToMany(targetEntity=PostReviews::class, mappedBy="postId")
     */
    private $postReviewId;

    /**
     * @ORM\ManyToOne(targetEntity=PostType::class, inversedBy="postId")
     * @ORM\JoinColumn(nullable=true)
     */
    private $postTypeId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $postImg;

    public function __construct()
    {
        $this->postReviewId = new ArrayCollection();
        $this->datePosted = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPostTitle(): ?string
    {
        return $this->postTitle;
    }

    public function setPostTitle(string $postTitle): self
    {
        $this->postTitle = $postTitle;

        return $this;
    }

    public function getPostContentSummary(): ?string
    {
        return $this->postContentSummary;
    }

    public function setPostContentSummary(string $postContentSummary): self
    {
        $this->postContentSummary = $postContentSummary;

        return $this;
    }

    public function getPostContentFull(): ?string
    {
        return $this->postContentFull;
    }

    public function setPostContentFull(string $postContentFull): self
    {
        $this->postContentFull = $postContentFull;

        return $this;
    }

    public function getPostLikes(): ?int
    {
        return $this->postLikes;
    }

    public function setPostLikes(?int $postLikes): self
    {
        $this->postLikes = $postLikes;

        return $this;
    }

    public function getPostDislikes(): ?int
    {
        return $this->postDislikes;
    }

    public function setPostDislikes(?int $postDislikes): self
    {
        $this->postDislikes = $postDislikes;

        return $this;
    }

    public function getDatePosted(): ?\DateTimeInterface
    {
        return $this->datePosted;
    }

    public function setDatePosted(\DateTimeInterface $datePosted): self
    {
        $this->datePosted = $datePosted;

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->userId;
    }

    public function setUserId(?User $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @return Collection|PostReviews[]
     */
    public function getPostReviewId(): Collection
    {
        return $this->postReviewId;
    }

    public function addPostReviewId(PostReviews $postReviewId): self
    {
        if (!$this->postReviewId->contains($postReviewId)) {
            $this->postReviewId[] = $postReviewId;
            $postReviewId->setPostId($this);
        }

        return $this;
    }

    public function removePostReviewId(PostReviews $postReviewId): self
    {
        if ($this->postReviewId->removeElement($postReviewId)) {
            // set the owning side to null (unless already changed)
            if ($postReviewId->getPostId() === $this) {
                $postReviewId->setPostId(null);
            }
        }

        return $this;
    }

    public function getPostTypeId(): ?PostType
    {
        return $this->postTypeId;
    }

    public function setPostTypeId(?PostType $postTypeId): self
    {
        $this->postTypeId = $postTypeId;

        return $this;
    }

    public function getPostImg(): ?string
    {
        return $this->postImg;
    }

    public function setPostImg(string $postImg): self
    {
        $this->postImg = $postImg;

        return $this;
    }
}
