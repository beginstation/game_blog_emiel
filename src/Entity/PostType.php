<?php

namespace App\Entity;

use App\Repository\PostTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PostTypeRepository::class)
 */
class PostType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $postTypeName;

    /**
     * @ORM\OneToMany(targetEntity=Post::class, mappedBy="postTypeId")
     */
    private $postId;


    public function __construct()
    {
        $this->postId = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPostTypeName(): ?string
    {
        return $this->postTypeName;
    }

    public function setPostTypeName(string $postTypeName): self
    {
        $this->postTypeName = $postTypeName;

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPostId(): Collection
    {
        return $this->postId;
    }

    public function addPostId(Post $postId): self
    {
        if (!$this->postId->contains($postId)) {
            $this->postId[] = $postId;
            $postId->setPostTypeId($this);
        }

        return $this;
    }

    public function removePostId(Post $postId): self
    {
        if ($this->postId->removeElement($postId)) {
            // set the owning side to null (unless already changed)
            if ($postId->getPostTypeId() === $this) {
                $postId->setPostTypeId(null);
            }
        }

        return $this;
    }

}
