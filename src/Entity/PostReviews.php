<?php

namespace App\Entity;

use App\Repository\PostReviewsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PostReviewsRepository::class)
 */
class PostReviews
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $reviewContent;

    /**
     * @ORM\ManyToOne(targetEntity=Post::class, inversedBy="postReviewId")
     */
    private $postId;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReviewContent(): ?string
    {
        return $this->reviewContent;
    }

    public function setReviewContent(string $reviewContent): self
    {
        $this->reviewContent = $reviewContent;

        return $this;
    }

    public function getPostId(): ?Post
    {
        return $this->postId;
    }

    public function setPostId(?Post $postId): self
    {
        $this->postId = $postId;

        return $this;
    }

}
